/*jshint latedef:false */
var path = require('path'),
  util = require('util'),
  yeoman = require('yeoman-generator'),
  scriptBase = require('../script-base');

module.exports = Generator;

function Generator() {
  scriptBase.apply(this, arguments);
  var dirPath = this.options.coffee ? '../templates/coffeescript/' : '../templates';
  this.sourceRoot(path.join(__dirname, dirPath));
}

util.inherits(Generator, scriptBase);

Generator.prototype.createViewFiles = function createViewFiles() {
  var ext = this.options.coffee ? 'coffee' : 'js';
  var templateFramework = this.getTemplateFramework();
  var templateExt = '.hbs';

  if (templateFramework === 'mustache') {
    templateExt = '-template.mustache';
  } else if (templateFramework === 'lodash') {
    templateExt = '.ejs';
  }

  this.jst_path = path.join('app/scripts/templates', this.name + templateExt);
  var destFile = path.join('app/scripts/views', this.name + '-view.' + ext);
  var isRequireJsApp = this.isUsingRequireJS();

  this.template('view.html', this.jst_path);

  if (templateFramework === 'mustache') {
    this.jst_path = this.name + '-template';
  }

  if (!isRequireJsApp) {
    this.template('view.' + ext, destFile);
    return;
  }
  this.requirejs_boilerplate = this.read('requirejs_boilerplate.' + ext, 'utf-8');
  this.template('requirejs_view.' + ext, destFile);

};
