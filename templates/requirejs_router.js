/*global define*/
define(function (require) {

    <%= requirejs_boilerplate %>
    var <%= _.classify(name) %>Router = Backbone.Router.extend({
        routes: {
        }
    });

    return <%= _.classify(name) %>Router;
});
