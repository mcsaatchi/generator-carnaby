/*global define*/
define(function (require) {

    <%= requirejs_boilerplate %>
    var JST = require('templates');

    var <%= _.classify(name) %>View = Backbone.View.extend({
        template: JST['<%= jst_path %>']
    });

    return <%= _.classify(name) %>View;
});
