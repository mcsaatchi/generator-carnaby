/*global require*/
'use strict';

require.config({
    shim: {
        underscore: {
            exports: '_'
        },
        marionette: {
            deps: [
                'underscore',
                'jquery',
                'backbone'
            ],
            exports: 'Backbone'
        },
        backbone: {
            deps: [
                'underscore',
                'jquery'
            ],
            exports: 'Backbone'
        }<% if (compassBootstrap) { %>,
        bootstrap: {
            deps: ['jquery'],
            exports: 'jquery'
        }<% } %><% if (templateFramework === 'handlebars') { %>,
        handlebars: {
            exports: 'Handlebars'
        }<% } %>
    },
    paths: {
        jquery: '../bower_components/jquery/jquery',
        backbone: '../bower_components/backbone-amd/backbone',
        marionette: '../bower_components/backbone.marionette/lib/backbone.marionette',
        underscore: '../bower_components/underscore-amd/underscore'<% if (compassBootstrap) { %>,
        bootstrap: 'vendor/bootstrap'<% } %><% if (templateFramework === 'handlebars') { %>,
        handlebars: '../bower_components/handlebars/handlebars'<% } %>
    }
});

require([
    'backbone',
    'marionette'
], function (Backbone, Marionette) {
    Backbone.history.start();
    console.log(Marionette);
});
