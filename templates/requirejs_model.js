/*global define*/
define(function (require) {

    <%= requirejs_boilerplate %>
    var <%= _.classify(name) %>Model = Backbone.Model.extend({
        defaults: {

        }
    });

    return <%= _.classify(name) %>Model;
});
