/*global define*/
define(function (require) {

    <%= requirejs_boilerplate %>
    var <%= _.classify(name) %>Collection = Backbone.Collection.extend({
        model: <%= _.classify(name) %>Model
    });

    return <%= _.classify(name) %>Collection;
});
